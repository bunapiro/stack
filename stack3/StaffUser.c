#include <stdio.h>
#include <stdbool.h>
#include "../../../include/local.h"
#include "Staff.h"

PRIVATE void CheckStackWithRangeValidator( void )
{
    int buf[16];
    int popVal = 0;

    Range range         = { 0, 9 };
    Validator validator = rangeValidator( &range );
    Stack stack         = newStackWithValidator( buf, &validator );

    push( &stack, 5 );
    pop( &stack, &popVal );

    printf("\n [RangeValidate] PopValue = %d" ,popVal );
}

PRIVATE void CheckStackWithPreviousValueValidator( void )
{
    int buf[16];
    int popVal = 0;

    PreviousValue previous  = { 0 };
    Validator validator     = previousValidator( &previous );
    Stack stack             = newStackWithValidator( buf, &validator );

    push( &stack, 123 );
    pop( &stack, &popVal );

    printf("\n [PreviousValidate] PopValue = %d" ,popVal );
}

PRIVATE void CheckStack( void )
{
    int buf[16];
    int popVal = 0;

    Stack stack = newStack( buf );

    push( &stack, 99 );
    pop( &stack, &popVal );

    printf("\n [NonValidate] PopValue = %d" ,popVal );
}

PUBLIC void main ()
{
    CheckStack();
    CheckStackWithRangeValidator();
    CheckStackWithPreviousValueValidator();
}

