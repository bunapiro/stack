#ifndef _STACK_H_
#define _STACK_H_


#ifdef __cplusplus
extern "C" {
#endif

/*********************************************************
Structure's
*********************************************************/

typedef struct {
    int     id;
    char*   name;
} Staff;

PUBLIC void Staff_init( Staff* this, int id, const char* name );
PUBLIC int Staff_getId( Staff* this );

#ifdef __cplusplus
}
#endif

#endif /* _STACK_H_ */
