# compile
$ make Stack <br>
$ make Staff    // for stack3

# execute
$ ./Stack.exe <br>
$ ./Staff.exe   // for stack3

# clean
$ make Clean
