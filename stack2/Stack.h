#ifndef _STACK_H_
#define _STACK_H_

//#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/*********************************************************
Structure's
*********************************************************/

/********************************************************
@brief 制約構造体
@param		(*const validate)()		:制約確認APIへのポインタ
@note
	 -
*********************************************************/
typedef struct Validator {
    bool (*const validate)(struct Validator *pThis, int val);
} Validator;
/********************************************************
@brief スタック格納範囲構造体
@param		base	:
@param		min		:スタックに格納できる最小値
@param		max	    :スタックに格納できる最大値
@note
	 -
*********************************************************/
typedef struct {
    Validator base;
    const int min;
    const int max;
} RangeValidator;
/********************************************************
@brief 構造体
@param		base		        :
@param		needOddEvenCheck	:
@param		needToBeOdd	        :
@note
	 -
*********************************************************/
typedef struct {
    RangeValidator  base;
    const bool      needOddEvenCheck;
    const bool      needToBeOdd;
} OddEvenRangeValidator;
/********************************************************
@brief スタック前回格納値構造体
@param		base		        :
@param		previousValue		:前回スタックに格納した値
@note
	 -
*********************************************************/
typedef struct {
    Validator   base;
    int         previousValue;
} PreviousValueValidator;
/********************************************************
@brief スタック構造体
@param		top				    :積まれているスタックの高さ
@param		const size			:スタック容量
@param		*const pBuf			:スタックバッファ               ※ポインタ変数への書き込み不可、実態領域への書き込み可
@param		*const pValidator	:スタックに格納できる値の制約   ※ポインタ変数への書き込み不可、実態領域への書き込み可
@note
	 -
*********************************************************/
typedef struct {
    int                 top;
    const size_t        size;
    int *const          pBuf;
    Validator *const    pValidator;
} Stack;

/*********************************************************
ProtoType
*********************************************************/
bool validateRange( Validator *pThis, int val );
bool validatePrevious( Validator *pThis, int val );
bool push( Stack *p, int val );
bool pop( Stack *p, int *pRet );

/*********************************************************
Define
*********************************************************/

/********************************************************
@brief スタックオブジェクトクリエイト
@param		buf		:スタックバッファへのポインタ
@note
	 - 制約無しスタックを生成する
*********************************************************/
#define newStack( buf ) {                   \
    0,                                      \
    sizeof(buf) / sizeof(int),              \
    (buf),                                  \
    NULL                                    \
}
/********************************************************
@brief スタックオブジェクトクリエイト
@param		buf		:スタックバッファへのポインタ
@note
	 - 制約有りスタックを生成する
*********************************************************/
#define newStackWithValidator( buf, pValidator ) {  \
    0,                                              \
    sizeof(buf) / sizeof(int),                      \
    (buf),                                          \
    (Validator *)(pValidator)                       \
}
/********************************************************
@brief 上下限制約オブジェクトクリエイト
@param		min		:スタックに格納できる最小値
@param		max	    :スタックに格納できる最大値
@note
	 - 上下限制約オブジェクトを生成する
*********************************************************/
#define newRangeValidator( min, max ) {         \
    {validateRange},                            \
    (min),                                      \
    (max)                                       \
}
/********************************************************
@brief 上下限制約オブジェクトクリエイト
@param		min		:スタックに格納できる最小値
@param		max	    :スタックに格納できる最大値
@note
	 - 上下限制約オブジェクトを生成する
*********************************************************/
#define newOddEvenRangeValidator( RangeValidator, needOddEvenCheck, needToBeOdd ) {     \
    (RangeValidator),                                                                   \
    (needOddEvenCheck),                                                                 \
    (needToBeOdd)                                                                       \
}
/********************************************************
@brief 前回格納値制約オブジェクトクリエイト
@param		default		:前回格納値の初期値
@note
	 - 前回格納値制約オブジェクトを生成する
*********************************************************/
#define newPreviousValidator( default ) {       \
    {validatePrevious},                         \
    default                                     \
}

#ifdef __cplusplus
}
#endif

#endif /* _STACK_H_ */
