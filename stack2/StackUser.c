#include <stdio.h>
#include <stdbool.h>
#include "../../../include/local.h"
#include "Stack.h"

PRIVATE void CheckStackWithRangeValidator( void )
{
    int buf[16];
    int popVal = 0;

    RangeValidator validator    = newRangeValidator( 0, 9 );
    Stack stack                 = newStackWithValidator( buf, &validator.base );

    push( &stack, 6 );
    pop( &stack, &popVal );

    printf("\n [RangeValidate] PopValue = %d" ,popVal );
}

PRIVATE void CheckStackWithPreviousValueValidator( void )
{
    int buf[16];
    int popVal = 0;

    PreviousValueValidator validator    = newPreviousValidator( 0 );
    Stack stack                         = newStackWithValidator( buf, &validator.base );

    push( &stack, 123 );
    pop( &stack, &popVal );

    printf("\n [PreviousValidate] PopValue = %d" ,popVal );
}

PRIVATE void CheckStack( void )
{
    int buf[16];
    int popVal = 0;

    Stack stack = newStack( buf );

    push( &stack, 99 );
    pop( &stack, &popVal );

    printf("\n [NonValidate] PopValue = %d" ,popVal );
}

PRIVATE void CheckStackWithOddEvenRangeValidator( void )
{
    int buf[16];
    int popVal = 0;

    RangeValidator RangeValidator   = newRangeValidator( 0, 9 );
    OddEvenRangeValidator validator = newOddEvenRangeValidator( RangeValidator, true, true );

    Stack stack                     = newStackWithValidator( buf, &validator.base );

    push( &stack, 8 );
    pop( &stack, &popVal );

    printf("\n [OddEvenRangeValidate] PopValue = %d" ,popVal );
}

PUBLIC void main ( void )
{
    CheckStack();
    CheckStackWithRangeValidator();
    //CheckStackWithOddEvenRangeValidator();
    CheckStackWithPreviousValueValidator();
}

