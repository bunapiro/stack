#include <stdio.h>
#include <stdbool.h>
#include "../../../include/local.h"
#include "Stack.h"

/*********************************************************
API's
*********************************************************/

/********************************************************
@brief スタックフルチェック
@param[in]		*p				:スタックオブジェクトへのポインタ
@return
				true			:スタックフル
				false			:スタックフルでない
@note
	 - オブジェクトpのスタックがフルか否かを判定する
*********************************************************/
PRIVATE bool isStackFull( const Stack *p )
{
    return p->top == p->size;
}
/********************************************************
@brief スタック空チェック
@param[in]		*p				:スタックオブジェクトへのポインタ
@return
				true			:スタックが空
				false			:スタックは空でない
@note
	 - オブジェクトpのスタックが空か否かを判定する
*********************************************************/
PRIVATE bool isStackEmpty( const Stack *p )
{
    return p->top == 0;
}
/********************************************************
@brief 範囲確認
@param[in]		*p			    :制約確認オブジェクトへのポインタ
@param[in]		val				:確認対象となる値
@return
				true			:格納上下限値制約を満たしている
				false			:格納上下限値制約を満たしていない
@note
	 - 入力値valが制約確認オブジェクトpThisの範囲制約である上下限値を満たしているか否かを判定する
*********************************************************/
PUBLIC bool validateRange( Validator *p, int val )
{
    OddEvenRangeValidator *pSuper   = (OddEvenRangeValidator*)p;
    RangeValidator *pThis           = (RangeValidator*)p;

    if ( pSuper->needToBeOdd ) {
        if ( IS_MULTIPLE_OF_2( val ) ) {
            return false;
        }

    }

    return pThis->min <= val && val <= pThis->max;
}
/********************************************************
@brief 範囲確認
@param[in]		*p			    :制約確認オブジェクトへのポインタ
@param[in]		val				:確認対象となる値
@return
				true			:前回格納値よりも大きい
				false			:前回格納値よりも小さい
@note
	 - 入力値valが制約オブジェクトpThisの範囲制約である前回格納値よりも大きいか否かを判定する
*********************************************************/
PUBLIC bool validatePrevious( Validator *p, int val )
{
    PreviousValueValidator *pThis = (PreviousValueValidator*)p;

    if ( val < pThis->previousValue ) return false;

    pThis->previousValue = val;

    return true;
}
/********************************************************
@brief スタック格納制約確認
@param[in]		*pThis			:制約確認オブジェクトへのポインタ
@param[in]		val				:確認対象となる値
@return
				true			:範囲制約を満たしている(または制約がない)
				false			:範囲制約を満たしていない
@note
	 - 入力値valが制約確認オブジェクトpThisの範囲制約を満たしているか否かを判定する
*********************************************************/
PUBLIC bool validate( Validator *p, int val )
{
    /* 制約確認関数へのポインタがNULL = 制約確認する必要がないスタックである */
    if ( !p ) return true;

    return p->validate( p, val );
}
/********************************************************
@brief スタックプッシュ
@param[in]		*p			:スタックオブジェクトへのポインタ
@param[in]		val			:スタックバッファへプッシュする値
@return
				true		:プッシュ成功
				false		:プッシュ失敗
@note
	 - 入力値valをスタックオブジェクトpのバッファへプッシュする
*********************************************************/
PUBLIC bool push( Stack *p, int val )
{
    if ( ! validate( p->pValidator, val ) || isStackFull( p ) ) return false;

    p->pBuf[p->top++] = val;

    return true;
}
/********************************************************
@brief スタックポップ
@param[in]		*p			:スタックオブジェクトへのポインタ
@param[out]		*pRet		:スタックバッファからポップされた値
@return
				true		:ポップ成功
				false		:ポップ失敗
@note
	 - スタックオブジェクトpのバッファから値をポップする
*********************************************************/
PUBLIC bool pop( Stack *p, int *pRet )
{
    if ( isStackEmpty( p ) ) return false;

    *pRet = p->pBuf[--p->top];

    return true;
}
